import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';

import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { SuccefullAlertComponent } from './components/succefull-alert/succefull-alert.component';
import { ErrorAlertComponent } from './components/error-alert/error-alert.component';
import { ConfirmAlertComponent } from './components/confirm-alert/confirm-alert.component';
import { FeedComponent } from './pages/feed/feed.component';
import { SharedRoutingModule } from './shared-routing.module';
import { ChatVehicleComponent } from './components/chat-vehicle/chat-vehicle.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { EmptyPostsComponent } from './components/empty-posts/empty-posts.component';

@NgModule({
  declarations: [
    MenuComponent,
    FooterComponent,
    SuccefullAlertComponent,
    ErrorAlertComponent,
    ConfirmAlertComponent,
    FeedComponent,
    ChatVehicleComponent,
    EmptyPostsComponent
  ],
  imports: [
    CommonModule,
    RouterLink,
    SharedRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    MenuComponent,
    FooterComponent
  ]
})
export class SharedModule { }