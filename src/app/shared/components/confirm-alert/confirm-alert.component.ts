import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { switchMap } from 'rxjs';

import { DataService } from 'src/app/admin/services/data/data.service';
import { VehiclesService } from 'src/app/admin/services/vehicles/vehicles.service';
import { WorkshopService } from 'src/app/admin/services/workshop/workshop.service';
import { AlertService } from '../../services/alerts/alert.service';
import { PostService } from '../../services/posts/post.service';
import { RepairService } from 'src/app/admin/services/repairs/repair.service';
import { RepairWorkshopService } from 'src/app/workshop/services/repair-workshop/repair-workshop.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-alert',
  templateUrl: './confirm-alert.component.html',
  styleUrls: ['./confirm-alert.component.css']
})
export class ConfirmAlertComponent implements OnInit{
  
  currentMode : Boolean = false;
  id : String | null = '';
  plates : String | null = '';
  id_workshop : String | null = '';
  id_vehicle : String | null = '';
  id_repair : String | null = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data : { modelo : string},
    private vehicle : VehiclesService,
    private workshop : WorkshopService,
    private _data : DataService,
    public dialogRef : MatDialogRef<ConfirmAlertComponent>,
    private alert : AlertService,
    private post : PostService,
    private repair : RepairService,
    private router : Router,
    private repair_workshop : RepairWorkshopService
  ){}

  ngOnInit(): void {
    this.darkMode();
    this.getID();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getID(){
    if(this.data.modelo === 'vehicle'){
      this.id = this._data.getIdVehicle();

    }else if(this.data.modelo === 'workshop'){
      this.id = this._data.getIdWorkshop();

    }else if(this.data.modelo === 'repair'){
      this.id_workshop = this._data.getIdWorkshop();
      this.id = this._data.getIdRepair();

    }else if(this.data.modelo === 'vehicle-arrive'){
      this.id = this._data.getIdRepair();

    }else if(this.data.modelo === 'confirm-repair'){
      this.id_repair = this.repair_workshop.getIdRepair();
      this.id_vehicle = this.repair_workshop.getIdVehicle();
    }

    this.plates = this._data.getPlates();
  }

  deleteVehicle(){
    this.vehicle.deleteVehicle(this.id).subscribe((data : any) => {
      if(data.ok === true){
        this.alert.showOk(data.message);

        this.dialogRef.close();
        
        setTimeout(() => {
          window.location.reload();
        }, 2001);
      }
    });
  }

  deleteWorkshop(){
    this.workshop.deleteWorkshop(this.id).subscribe((data : any) => {
      if(data.ok === true){
        this.alert.showOk(data.message);

        this.dialogRef.close();
        
        setTimeout(() => {
          window.location.reload();
        }, 2001);
      }
    });
  }

  deleteRepair(){
    this.repair.deleteRepairId(this.id, this.plates, this.id_workshop).pipe(
      switchMap(() => this.post.deletePostRepair(this.id))
    ).subscribe((data : any) => {
      this.alert.showOk('Reparacion y publicacion eliminadas');
      setTimeout(() => {
        window.location.reload();
      }, 2001);
    }, (err : any) => {
      this.alert.showError(err.error.message);
    });
    setTimeout(() => {
      window.location.reload();
    }, 2001);
  }

  confirmArrival(){
    const body = {
      Id_Reparacion : this.id
    }

    this.repair_workshop.confirmArrival(body).subscribe((data : any) => {
      if(data.ok === true){
        this.alert.showOk(data.message);

        setTimeout(() => {
          window.location.reload();
        },2100);
      }
    }, (err : any ) => {
      this.alert.showError(err.error.message);
    })
  }

  confirmRepair(){
    this.repair.confirmRepair(this.id_repair, this.id_vehicle).subscribe((data : any) => {
      if(data.ok === true){
        this.alert.showOk(data.message);

        setTimeout(() => {
          window.location.reload();
        },2100);
      }
    }, (err : any ) => {
      this.alert.showError(err.error.message);
    });
  }

  delete(){
    if(this.data.modelo === 'vehicle'){
      this.deleteVehicle();
    }else if(this.data.modelo === 'workshop'){
      this.deleteWorkshop();
    }else if(this.data.modelo === 'repair'){
      this.deleteRepair();
    }else if(this.data.modelo === 'vehicle-arrive'){
      this.confirmArrival();
    }else if(this.data.modelo === 'confirm-repair'){
      this.confirmRepair();
    }
  }

  cancel(){
    this.dialogRef.close();
  }

}