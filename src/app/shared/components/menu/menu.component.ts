import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit{

  isAuthenticated: boolean = false;
  userType: string = ''; 

  constructor(
    private auth : AuthService
  ){}

  ngOnInit(): void {
    this.auth.isAuthenticated().subscribe(authenticated => {
      this.isAuthenticated = authenticated;
    });

    this.auth.getUserType().subscribe((userType) => {
      this.userType = userType;
    });
  }

  logOut(){
    this.auth.logOut();
  }

}