import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empty-posts',
  templateUrl: './empty-posts.component.html',
  styleUrls: ['./empty-posts.component.css']
})
export class EmptyPostsComponent implements OnInit{

  currentMode : Boolean | null = false;

  ngOnInit(): void {
    this.darkMode();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

}
