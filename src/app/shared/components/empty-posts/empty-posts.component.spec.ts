import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyPostsComponent } from './empty-posts.component';

describe('EmptyPostsComponent', () => {
  let component: EmptyPostsComponent;
  let fixture: ComponentFixture<EmptyPostsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EmptyPostsComponent]
    });
    fixture = TestBed.createComponent(EmptyPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
