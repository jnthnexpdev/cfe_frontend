import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccefullAlertComponent } from './succefull-alert.component';

describe('SuccefullAlertComponent', () => {
  let component: SuccefullAlertComponent;
  let fixture: ComponentFixture<SuccefullAlertComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SuccefullAlertComponent]
    });
    fixture = TestBed.createComponent(SuccefullAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
