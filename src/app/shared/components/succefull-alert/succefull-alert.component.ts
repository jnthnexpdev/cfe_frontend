import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-succefull-alert',
  templateUrl: './succefull-alert.component.html',
  styleUrls: ['./succefull-alert.component.css']
})
export class SuccefullAlertComponent implements OnInit{
  currentMode: boolean = false;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: { message: string }) {}

  ngOnInit(): void {
    this.darkMode();
  }
  
  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

}