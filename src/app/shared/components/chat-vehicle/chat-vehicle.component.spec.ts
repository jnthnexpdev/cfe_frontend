import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatVehicleComponent } from './chat-vehicle.component';

describe('ChatVehicleComponent', () => {
  let component: ChatVehicleComponent;
  let fixture: ComponentFixture<ChatVehicleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChatVehicleComponent]
    });
    fixture = TestBed.createComponent(ChatVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
