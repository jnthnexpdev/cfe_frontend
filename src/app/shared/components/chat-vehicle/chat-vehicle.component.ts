import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from '../../services/posts/post.service';
import { AlertService } from '../../services/alerts/alert.service';
import { AuthService } from 'src/app/auth/services/auth/auth.service';
import { CommunicationService } from '../../services/communication/communication.service';
import { SystemService } from '../../services/system/system.service';

@Component({
  selector: 'app-chat-vehicle',
  templateUrl: './chat-vehicle.component.html',
  styleUrls: ['./chat-vehicle.component.css', './chat-extra.css'],
})
export class ChatVehicleComponent implements OnInit{

  currentMode: boolean = false;
  adminsData : any [] = [];
  vehiclesData : any [] = [];
  postsData : any = [];
  commentsData : any = {};
  currentPage = 1;
  totalPages : number = 0;
  departmentFeed : string | null = null;

  respuestas : string[] = [];
  id : string | null = '';
  searchValue: string = '';

  constructor(
    private post : PostService,
    private router : Router,
    private alert : AlertService,
    private auth : AuthService,
    private update : CommunicationService,
    private system : SystemService
  ){}

  ngOnInit(): void {
    this.darkMode();
    this.getDepartment();
  
    this.update.updateComponent$.subscribe(() => {
      this.getDepartment();
    });
  
    this.system.currentSearchValue.subscribe((value) => {
      this.searchValue = value;
      if (this.searchValue === '') {
        // Si no hay término de búsqueda, verifica el departamento
        if (this.departmentFeed === 'All') {
          // Mostrar todos los posts
          this.getPosts();
        } else {
          // Mostrar posts del departamento seleccionado
          this.getPostsDepartment();
        }
      } else {
        // Hay un término de búsqueda, mostrar resultados de búsqueda
        this.getPostsBySearch();
      }
    });
  }
  

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getId(){
    this.id = this.auth.getId();
  }

  async getPosts() {
    try {
      const data = await this.post.getPosts(this.currentPage).toPromise();
      this.adminsData = data.admins;
      this.vehiclesData = data.vehicles;
      this.postsData = data.posts;
      this.commentsData = data.users;
      this.totalPages = data.totalPages;
      this.system.setNullPosts('posts');
    } catch (err) {
      this.system.setNullPosts('vacio');
      // Manejar errores aquí si es necesario
    }
  }
  

  getPostsDepartment(){
    this.post.departmentPosts(this.departmentFeed).subscribe((data : any) => {
      this.adminsData = data.admins;
      this.vehiclesData = data.vehicles;
      this.postsData = data.posts;
      this.commentsData = data.users;
      this.totalPages = data.totalPages;
      this.system.setNullPosts('posts');
    }, (err : any) => {
      this.system.setNullPosts('vacio');
    });
  }

  getPostsBySearch(){
    this.post.postsByPlates(this.currentPage, this.searchValue).subscribe((data : any) => {
      this.adminsData = data.admins;
      this.vehiclesData = data.vehicles;
      this.postsData = data.posts;
      this.commentsData = data.users;
      this.totalPages = data.totalPages;
      this.system.setNullPosts('posts');
    }, (err : any) => {
      this.system.setNullPosts('vacio');
    });
  }

  getDepartment() {
    this.departmentFeed = this.system.getDepartment();

    if (this.departmentFeed === 'All') {
      this.getPosts();
    } else {
      this.getPostsDepartment();
    }
  }

  async addComment(index : number, id_post : any){
    this.getId();

    const body = {
      Id_Usuario : this.id,
      Contenido : this.respuestas[index]
    };

    this.post.addComment(id_post, body).subscribe((data : any) => {
      if(data.ok === true){
        this.alert.showOk('Comentario agregado');
        this.update.updatePosts();
        this.respuestas[index] = '';
      }
    }, (err : any) => {
      this.alert.showError(err.error.message);
    });
  }

  previousPage(){
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getPosts();
    }
  }

  nextPage(){
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.getPosts();
    }
  }

  isPreviousDisabled(): string | null {
    return this.currentPage === 1 ? 'disabled-button' : null;
  }
  
  isNextDisabled(): string | null {
    return this.currentPage === this.totalPages ? 'disabled-button' : null;
  }
 
}