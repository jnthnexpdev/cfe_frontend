import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeedComponent } from './pages/feed/feed.component';
import { AuthGuard } from '../auth/guards/auth.guard';

const routes: Routes = [
  {
    path : 'feed',
    children : [
      {path : 'publicaciones', component : FeedComponent},
      {path : '**', redirectTo : 'publicaciones', pathMatch : 'full'}
    ],
    canActivate : [AuthGuard]
  }, 
  {
    path : '**',
    redirectTo : 'feed',
    pathMatch : 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }