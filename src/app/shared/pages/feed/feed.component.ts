import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/posts/post.service';
import { SystemService } from '../../services/system/system.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit{
  currentMode: boolean = false;
  departmentsData : any [] = [];
  stateFeed : string | null = null;
  departmentFeed : string | null = null;
  searchValue: string = '';

  constructor(
    private post : PostService,
    private system : SystemService
  ){}

  ngOnInit(): void {
    this.darkMode();
    this.getDepartments();
    this.getDepartment();
    this.getStateFeed();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getDepartment(){
    this.departmentFeed = this.system.getDepartment();
  }

  getStateFeed(){
    this.stateFeed = this.system.getStateFeed();
  }

  getDepartments(){
    this.post.countPosts().subscribe((data : any) => {
      this.departmentsData = data.departamentos;
    });
  }

  setDepartment(value : any){
    this.system.setDepartment(value);
  }

  allPosts(){
    this.system.setDepartment('All');
  }

  onSearchInputChange() {
    this.system.setSearchValue(this.searchValue);
  }
  
}