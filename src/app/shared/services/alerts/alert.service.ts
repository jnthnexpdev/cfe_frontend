import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ErrorAlertComponent } from '../../components/error-alert/error-alert.component';
import { SuccefullAlertComponent } from '../../components/succefull-alert/succefull-alert.component';
import { ConfirmAlertComponent } from '../../components/confirm-alert/confirm-alert.component';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  
  constructor(private dialog : MatDialog) { }

  showError(message : String, duration: number = 2000): void{
    const dialogRef : MatDialogRef <ErrorAlertComponent> = this.dialog.open(ErrorAlertComponent, {
      data: {message : message}
    });

    setTimeout(() => {
      dialogRef.close();
    }, duration);
  }

  showOk(message : String, duration : number = 2000): void{
    const dialogRef : MatDialogRef <SuccefullAlertComponent> = this.dialog.open(SuccefullAlertComponent, {
      data : {message : message}
    });

    setTimeout(() => {
      dialogRef.close();
    }, duration);
  }

  showConfirm(modelo : String): void{
    const dialogRef : MatDialogRef <ConfirmAlertComponent> = this.dialog.open(ConfirmAlertComponent, {
      data : {modelo : modelo}
    });
  }

}