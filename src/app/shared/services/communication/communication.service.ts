import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private updateComponent = new Subject<void>();

  updateComponent$ = this.updateComponent.asObservable();

  updatePosts() {
    this.updateComponent.next();
  }

}