import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SystemService {

  public feedState = new BehaviorSubject<string | null>(null);
  public departmentFeed = new BehaviorSubject<string | null>(null);
  private searchValue = new BehaviorSubject<string>('');
  currentSearchValue = this.searchValue.asObservable();

  constructor() {}

  //Estado cuando no hayan publicaciones aun creadas
  setNullPosts(estado : any){
    localStorage.setItem('status_feed', estado);
  }

  getStateFeed(){
    return localStorage.getItem('status_feed');
  }

  setDepartment(department : any){
    localStorage.setItem('department', department);
    setTimeout(() => {
      window.location.reload();
    },1);
  }

  getDepartment(){
    return localStorage.getItem('department');
  }

  setSearchValue(value: string) {
    this.searchValue.next(value);
  }

}