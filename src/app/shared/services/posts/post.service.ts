import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private url : String = 'http://localhost:3900/api/v1/cfe/post';

  constructor(
    private http : HttpClient
  ){}

  getPosts(page : number) : Observable<any>{
    const pageSize = 2
    const url = `${this.url}/all`;
    const options = { 
      withCredentials: true,
      params: {
        page: page.toString(),
        pageSize: pageSize.toString()
      }
    };
    return this.http.get(url, options);
  }

  getStatsPosts() : Observable<any>{
    const url = `${this.url}/active-posts`;
    const options = { 
      withCredentials: true,
    };
    return this.http.get(url, options);
  }

  departmentPosts( value : any) : Observable<any>{
    const url = `${this.url}/search/${value}`;
    const options = { 
      withCredentials: true,
    };
    return this.http.get(url, options);
  }

  postsByPlates(page : number, value : any) : Observable<any>{
    const pageSize = 2
    const url = `${this.url}/search-plates/${value}`;
    const options = { 
      withCredentials: true,
      params: {
        page: page.toString(),
        pageSize: pageSize.toString()
      }
    };
    return this.http.get(url, options);
  }

  addComment(id : any, body : any){
    const url = `${this.url}/add-comment/${id}`;
    const options = { withCredentials : true };
    return this.http.post(url, body, options);
  }

  deletePostId(id : any){
    const url = `${this.url}/delete/${id}`;
    const options = { withCredentials : true };
    return this.http.delete(url, options);
  }

  deletePostRepair(id : any){
    const url = `${this.url}/delete-repair/${id}`;
    const options = { withCredentials : true };
    return this.http.delete(url, options);
  }

  countPosts(){
    const url = `${this.url}/count`;
    const options = { withCredentials : true };
    return this.http.get(url, options);
  }

}
