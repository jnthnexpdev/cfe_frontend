import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewWorkshopsComponent } from './pages/view-workshops/view-workshops.component';
import { EditWorkshopComponent } from './pages/edit-workshop/edit-workshop.component';
import { AddWorkshopComponent } from './pages/add-workshop/add-workshop.component';
import { ViewVehiclesComponent } from './pages/view-vehicles/view-vehicles.component';
import { AddVehicleComponent } from './pages/add-vehicle/add-vehicle.component';
import { ViewRepairsComponent } from './pages/view-repairs/view-repairs.component';
import { EditVehicleComponent } from './pages/edit-vehicle/edit-vehicle.component';
import { AddRepairComponent } from './pages/add-repair/add-repair.component';
import { EditRepairComponent } from './pages/edit-repair/edit-repair.component';
import { ChartsInfoComponent } from './pages/charts-info/charts-info.component';

const routes: Routes = [
  {
    path : '',
    children : [
      {path : 'ver-talleres', component : ViewWorkshopsComponent},
      {path : 'ver-reparaciones', component : ViewRepairsComponent},
      {path : 'ver-vehiculos', component : ViewVehiclesComponent},
      {path : 'editar-taller/:id', component : EditWorkshopComponent},
      {path : 'editar-vehiculo/:id', component : EditVehicleComponent},
      {path : 'nuevo-taller', component : AddWorkshopComponent},
      {path : 'nuevo-vehiculo', component : AddVehicleComponent},
      {path : 'nueva-reparacion', component : AddRepairComponent},
      {path : 'editar-reparacion/:id', component : EditRepairComponent},
      {path: 'informe-reparaciones', component : ChartsInfoComponent},
      {path : '**', redirectTo : 'ver-talleres', pathMatch : 'full'}
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }