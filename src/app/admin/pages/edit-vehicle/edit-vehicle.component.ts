import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Data, Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { VehiclesService } from '../../services/vehicles/vehicles.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-edit-vehicle',
  templateUrl: './edit-vehicle.component.html',
  styleUrls: ['./edit-vehicle.component.css', './edit-extra.css']
})
export class EditVehicleComponent implements OnInit{

  vehicle : any = {};
  id_vehicle : string | null = '';
  currentMode : Boolean = false;
  isLinear = false;
  days = ['L', 'M', 'M', 'J', 'V', 'S', 'D'];
  daysValue : string[] = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
  selectedDays: string[] = [];

  departmentForm = this.form.group({
    Economico_Vehiculo : ['', Validators.required],
    Telefono_Encargado: ['', [Validators.required, Validators.pattern(/^(\+\d{1,3})?[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})$/)]],
    RPE_Resguardante: ['', Validators.required],
    Nombre_Resguardante: ['', Validators.required],
    Numero_Activo: ['', Validators.required],
    Division: ['', Validators.required],
    Zona: ['', Validators.required],
    Departamento_Vehiculo: ['', Validators.required],
    Area: ['', Validators.required],
    Centro_Trabajo: ['', Validators.required],
  });

  vehicleForm = this.form.group({
    Dias_Descanso: [''],
    Financiamiento_Vehiculo: ['', Validators.required],
    Placas_Vehiculo: ['', Validators.required],
    Traccion_Vehiculo: ['', Validators.required],
    Combustible_Vehiculo: ['', Validators.required],
    Uso_Vehiculo : ['', Validators.required],
    Estatus_Vehiculo: ['', Validators.required],
    Estatus_MYSAP_Vehiculo: ['', Validators.required],
    Ubicacion_Fisica_Vehiculo: ['', Validators.required],
    Verificacion_Vehiculo: ['', Validators.required],
    Problema_Verificacion_Vehiculo : ['', Validators.required],
  });

  chasisForm = this.form.group({
    Marca_Chasis: ['', Validators.required],
    Submarca_Chasis: ['', Validators.required],
    Modelo_Chasis: ['', Validators.required],
    Numero_Serie_Chasis: ['', Validators.required],
    Tipo_Chasis: ['', Validators.required],
    
    Marca_Hidraulico: ['', Validators.required],
    Tipo_Hidraulico: ['', Validators.required],
    Submarca_Hidraulico: ['', Validators.required],
    Modelo_Hidraulico : ['', Validators.required],
  });

  constructor(
    private form : FormBuilder,
    private router : Router,
    private alert : AlertService,
    private _vehicle : VehiclesService,
    private data : DataService
  ){}

  ngOnInit(): void {
    this.darkMode();
    this.getId();
    this.getDataVehicle();
    this.days.forEach(day => this.toggleDay(day));
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  toggleDay(day: string) {
    const index = this.selectedDays.indexOf(day);
  
    if (index !== -1) {
      this.selectedDays.splice(index, 1);
    } else {
      this.selectedDays.push(day);
    }
    this.vehicleForm.get('Dias_Descanso')?.setValue(this.selectedDays.join(', '));
  }
  

  getId(){
    this.id_vehicle = this.data.getIdVehicle();
  }

  backTable(){
    setTimeout(() => {
      this.router.navigate(['/administrador/ver-vehiculos']).then(() => {});
    }, 1);
  }

  getForms(){
    this.vehicle = {
      ...this.departmentForm.value,
      ...this.vehicleForm.value,
      ...this.chasisForm.value
    };

    this.editVehicle();
  }

  getDataVehicle(){
    this._vehicle.searchVehicleID(this.id_vehicle).subscribe((data : any) => {
      this.vehicle = data.vehicle;
      const savedDays = this.vehicle.Dias_Descanso ? this.vehicle.Dias_Descanso.split(', ') : [];


      this.departmentForm.patchValue({
        Economico_Vehiculo : this.vehicle.Economico_Vehiculo,
        Telefono_Encargado : this.vehicle.Telefono_Encargado,
        RPE_Resguardante : this.vehicle.RPE_Resguardante,
        Nombre_Resguardante : this.vehicle.Nombre_Resguardante,
        Numero_Activo: this.vehicle.Numero_Activo,
        Division : this.vehicle.Division,
        Zona: this.vehicle.Zona,
        Departamento_Vehiculo : this.vehicle.Departamento_Vehiculo,
        Area: this.vehicle.Area,
        Centro_Trabajo: this.vehicle.Centro_Trabajo,
      });

      this.vehicleForm.patchValue({
        Financiamiento_Vehiculo: this.vehicle.Financiamiento_Vehiculo,
        Placas_Vehiculo : this.vehicle.Placas_Vehiculo,
        Traccion_Vehiculo: this.vehicle.Traccion_Vehiculo,
        Combustible_Vehiculo: this.vehicle.Combustible_Vehiculo,
        Uso_Vehiculo: this.vehicle.Uso_Vehiculo,
        Estatus_Vehiculo: this.vehicle.Estatus_Vehiculo,
        Estatus_MYSAP_Vehiculo: this.vehicle.Estatus_MYSAP_Vehiculo,
        Ubicacion_Fisica_Vehiculo: this.vehicle.Ubicacion_Fisica_Vehiculo,
        Verificacion_Vehiculo: this.vehicle.Verificacion_Vehiculo,
        Problema_Verificacion_Vehiculo: this.vehicle.Problema_Verificacion_Vehiculo,
      });

      this.chasisForm.patchValue({
        Marca_Chasis : this.vehicle.Marca_Chasis,
        Submarca_Chasis : this.vehicle.Submarca_Chasis,
        Modelo_Chasis: this.vehicle.Modelo_Chasis,
        Numero_Serie_Chasis : this.vehicle.Numero_Serie_Chasis,
        Tipo_Chasis : this.vehicle.Tipo_Chasis,
        Marca_Hidraulico: this.vehicle.Marca_Hidraulico,
        Tipo_Hidraulico : this.vehicle.Tipo_Hidraulico,
        Submarca_Hidraulico: this.vehicle.Submarca_Hidraulico,
        Modelo_Hidraulico: this.vehicle.Modelo_Hidraulico,
      });

      this.selectedDays = savedDays;

    });
  }

  editVehicle(){
    console.log(this.id_vehicle);
    this._vehicle.editVehicle(this.id_vehicle, this.vehicle).subscribe((data : any) => {
      if(data.ok === true){
        this.alert.showOk(data.message);

        setTimeout(() => {
          this.router.navigate(['/administrador/ver-vehiculos']).then(() => {
            window.location.reload();
          });
        }, 2100);
      }
    }, (err : any) => {
      this.alert.showError(err.error.message);
    });
  }

}