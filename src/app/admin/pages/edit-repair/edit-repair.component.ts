import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth/auth.service';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { RepairService } from '../../services/repairs/repair.service';
import { WorkshopService } from '../../services/workshop/workshop.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-edit-repair',
  templateUrl: './edit-repair.component.html',
  styleUrls: ['./edit-repair.component.css']
})
export class EditRepairComponent {

  currentMode : Boolean = false;
  repairForm!: FormGroup;
  files: File[] = [];
  id : string | null = '';
  workshops : any [] = [];

  constructor(
    private router : Router,
    private alert : AlertService,
    private form : FormBuilder,
    private repair : RepairService,
    private workshop : WorkshopService,
    private data : DataService
  ){}

  ngOnInit(): void {
    this.repairForm = this.form.group({
      Id_Mecanico : ['', Validators.required],
      Presupuesto_Inicial: ['', [Validators.required, Validators.pattern(/^[0-9]+(?:\.\d+)?(?!e)[^e]*$/)]],
    });

    this.darkMode();
    this.getIdRepair();
    this.getWorkshops();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getIdRepair(){
    this.id = this.data.getIdRepair();
  }

  backTable(){
    setTimeout(() => {
      this.router.navigate(['/administrador/ver-reparaciones']).then(() => {});
    }, 1);
  }

  getWorkshops(){
    this.workshop.getNameWorkshops().subscribe((data : any) => {
      this.workshops = data.workshops;
      console.log(this.workshops);
    });
  }



  editRepair(){
    if(this.repairForm.valid){

      const updateRepair = this.repairForm.value;

      this.repair.updateRepair(this.id, updateRepair).subscribe((data : any) => {
        if(data.ok === true){
          this.alert.showOk(data.message);
          setTimeout(() => {
            this.router.navigate(['/administrador/ver-reparaciones']).then(() => {});
          }, 2100);
        }
      }, (err : any) => {
        this.alert.showError(err.error.message);
      });

    }else{
      this.alert.showError('Selecciona un taller y/o ingresa una cantidad valida');
    }
  }

}
