import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { RepairService } from '../../services/repairs/repair.service';
import { AuthService } from 'src/app/auth/services/auth/auth.service';
import { SystemService } from 'src/app/shared/services/system/system.service';

@Component({
  selector: 'app-add-repair',
  templateUrl: './add-repair.component.html',
  styleUrls: ['./add-repair.component.css']
})
export class AddRepairComponent implements OnInit{

  currentMode : Boolean = false;
  repairForm!: FormGroup;
  files: File[] = [];
  id : string | null = '';
  picker2 : string | null = null;

  constructor(
    private router : Router,
    private alert : AlertService,
    private form : FormBuilder,
    private repair : RepairService,
    private auth : AuthService,
    private datePipe: DatePipe,
    private system : SystemService
  ){}

  ngOnInit(): void {
    this.repairForm = this.form.group({
      Placas_Vehiculo : ['', Validators.required],
      Diagnostico_Previo : ['', Validators.required],
      Fecha_Problema: [this.datePipe.transform(new Date(), 'dd/MM/yyyy'), Validators.required],
      Imagenes_Vehiculo : ['', Validators.required],
      Id_Administrador : [''],
    });

    this.darkMode();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getID(){
    this.id = this.auth.getId();

    this.repairForm.patchValue({
      Id_Administrador : this.id
    });
  }

  backTable(){
    setTimeout(() => {
      this.router.navigate(['/administrador/ver-reparaciones']).then(() => {
        window.location.reload();
      });
    }, 1);
  }

  onFileChange(event: any): void {
    const selectedFiles = event.target.files;
  
    for (let i = 0; i < selectedFiles.length; i++) {
      this.files.push(selectedFiles[i]);
    }
  }
  

  removeFile(index: number): void {
    this.files.splice(index, 1);
  }

  addRepair(){
    this.getID();

    const fechaSeleccionada = this.repairForm.value.Fecha_Problema;

    if (fechaSeleccionada instanceof Date && !isNaN(fechaSeleccionada.getTime())) {
      const fechaFormateada = this.datePipe.transform(
        fechaSeleccionada,
        'dd/MM/yyyy'
      );

      const objetoAEnviar = {
        Placas_Vehiculo: this.repairForm.value.Placas_Vehiculo,
        Diagnostico_Previo: this.repairForm.value.Diagnostico_Previo,
        Fecha_Problema: fechaFormateada,
        Imagenes_Vehiculo: this.repairForm.value.Imagenes_Vehiculo,
        Id_Administrador: this.repairForm.value.Id_Administrador,
      };

      if(this.repairForm.valid){
        this.repair.addRepair(objetoAEnviar, this.files).subscribe((data : any) => {
          if(data.ok === true){
            this.alert.showOk(data.message);
          }
          setTimeout(() => {
            this.router.navigate(['/administrador/ver-reparaciones']).then(() => {
              window.location.reload();
            });
          }, 2100);
          this.system.setNullPosts('posts');
        }, (err : any) => {
          this.alert.showError(err.error.message);
        });
      }else{
        this.alert.showError('Verifica la informacion');
      }
      
    } else {
      this.alert.showError('Fecha no válida');
    }

  }
  
}