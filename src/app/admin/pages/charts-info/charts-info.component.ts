import { Component, OnInit,  ElementRef, ViewChild } from '@angular/core';
import { ChartData, ChartOptions, ChartType, CoreChartOptions, PluginOptionsByType } from 'chart.js';
import { RepairService } from '../../services/repairs/repair.service';
import { PostService } from 'src/app/shared/services/posts/post.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-charts-info',
  templateUrl: './charts-info.component.html',
  styleUrls: ['./charts-info.component.css']
})
export class ChartsInfoComponent implements OnInit{

  currentMode: Boolean = false;
  repairsData: any[] = [];
  postsData: any[] = [];
  activas: number = 0;
  concluidas: number = 0;
  pendientesData: any[] = [];

  @ViewChild('pdfContent') pdfContent!: ElementRef;

  public barChartType1: ChartType = 'pie';
  public barChartLabels1: string[] = [`En curso ${this.activas}`, `Concluidas ${this.concluidas}`];
  public barChartData1: ChartData<'bar'> = {
    labels: this.barChartLabels1,
    datasets: [
      {
        label: 'Vehiculos',
        data: [this.activas, this.concluidas],
        backgroundColor: ["#008e60", "#9abe26", "#140152", "#f45866", "#f2e30f", "#bb0a21", "#3f8efc"],
        hoverBackgroundColor: ["#008e60", "#9abe26", "#140152", "#f45866", "#f2e30f", "#bb0a21", "#3f8efc"],
      }
    ]
  };
  public barChartOptions1: ChartOptions = {
    responsive: true,
    plugins : {
      legend : {
        labels : {
          font : {
            family : 'Arial',
            size : 16,
            weight : 'bold'
          }
        }
      }
    }
  };

  public barChartType2: ChartType = 'pie';
  public barChartLabels2: string[] = []; // Etiquetas basadas en _id
  public barChartData2: ChartData<'bar'> = {
    labels: this.barChartLabels2,
    datasets: [
      {
        label: 'Pendientes',
        data: [], // Valores basados en count
        backgroundColor: ["#008e60", "#9abe26", "#140152", "#f45866", "#f2e30f", "#bb0a21", "#3f8efc"],
        hoverBackgroundColor: ["#008e60", "#9abe26", "#140152", "#f45866", "#f2e30f", "#bb0a21", "#3f8efc"],
      }
    ]
  };
  
  public barChartOptions2: ChartOptions = {
    responsive: true,
    plugins : {
      legend : {
        labels : {
          font : {
            family : 'Arial',
            size : 16,
            weight : 'bold'
          }
        }
      },
    }
  };

  constructor(
    private repair : RepairService,
    private post : PostService
  ){}

  ngOnInit(): void {
    this.darkMode();
    this.loadData();
  }

  darkMode() {
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  loadData() {
    this.repair.getStats().subscribe((data: any) => {
      this.activas = data.activas;
      this.concluidas = data.concluidas;
      this.updateChart1(); // Actualiza la primera gráfica después de obtener los datos
    });

    this.post.getStatsPosts().subscribe((data: any) => {
      this.pendientesData = data.pendientes;
      this.updateChart2(); // Actualiza la segunda gráfica después de obtener los datos
    });
  }

  updateChart1() {
    this.barChartLabels1 = [`En curso ${this.activas}`, `Concluidas ${this.concluidas}`];
    this.barChartData1.labels = this.barChartLabels1;
    this.barChartData1.datasets[0].data = [this.activas, this.concluidas];
  }

  updateChart2() {
    // Actualiza las etiquetas y los valores basados en _id y count
    this.barChartLabels2 = this.pendientesData.map(item => `${item._id} (${item.count})`);
    this.barChartData2.labels = this.barChartLabels2;
    this.barChartData2.datasets[0].data = this.pendientesData.map(item => item.count);
  }

  data2() {
    this.post.getStatsPosts().subscribe((data: any) => {
      this.pendientesData = data.pendientes;
      this.updateChart2(); // Actualiza la segunda gráfica después de obtener los datos
    });
  }

  generatePDF() {
    const DATA = document.getElementById('pdfContent');
    
    if (!DATA) {
      console.error('No se encontró el elemento con el ID "pdfContent".');
      return;
    }
    
    // Configurar opciones de html2canvas
    const options = {
      scale: 1,  // Ajusta el valor según sea necesario
      useCORS: true,  // Habilita el uso de CORS para imágenes externas
      logging: true  // Habilita el registro en la consola para ver posibles errores
    };
    
    // Crear instancia de jsPDF
    const doc = new jsPDF('p', 'pt', 'a4');
    
    // Capturar el contenido en un canvas
    html2canvas(DATA, options).then((canvas) => {
      // Obtener la imagen en formato base64
      const img = canvas.toDataURL('image/jpeg', 1.0);
    
      // Calcular el nuevo ancho y alto manteniendo la relación de aspecto
      const aspectRatio = canvas.width / canvas.height;
      const newWidth = Math.max(350, 350 * aspectRatio);  // Ajusta el valor según sea necesario
      const newHeight = newWidth / aspectRatio;
    
      // Calcular la posición para centrar horizontalmente y colocar en la parte superior
      const xPos = (doc.internal.pageSize.getWidth() - newWidth) / 2;
      const yPos = 20;  // Ajusta el valor según sea necesario (colocar en la parte superior)
    
      // Agregar la primera imagen al PDF centrada y en la parte superior
      doc.addImage(img, 'JPEG', xPos, yPos, newWidth, newHeight);
    
      // Agregar un salto de página
      doc.addPage();
    
      // Capturar el contenido de la segunda gráfica en un nuevo canvas
      // Asegúrate de tener una referencia al elemento HTML de la segunda gráfica
      const secondChartCanvas = document.getElementById('secondChart');
      if (!secondChartCanvas) {
        console.error('No se encontró el elemento con el ID "secondChart".');
        return;
      }
    
      // Capturar el contenido de la segunda gráfica en un canvas
      return html2canvas(secondChartCanvas, options).then((secondCanvas) => {
        // Obtener la imagen de la segunda gráfica en formato base64
        const secondImg = secondCanvas.toDataURL('image/jpeg', 1.0);
    
        // Calcular el nuevo ancho y alto manteniendo la relación de aspecto
        const secondAspectRatio = secondCanvas.width / secondCanvas.height;
        const secondNewWidth = Math.max(350, 350 * secondAspectRatio);  // Ajusta el valor según sea necesario
        const secondNewHeight = secondNewWidth / secondAspectRatio;
    
        // Calcular la posición para centrar horizontalmente y colocar en la parte superior
        const secondXPos = (doc.internal.pageSize.getWidth() - secondNewWidth) / 2;
        const secondYPos = 20;  // Ajusta el valor según sea necesario (colocar en la parte superior)
    
        // Agregar la segunda imagen al PDF centrada y en la parte superior
        doc.addImage(secondImg, 'JPEG', secondXPos, secondYPos, secondNewWidth, secondNewHeight);
    
        const currentDate = new Date().toLocaleDateString().replace(/\//g, '-');
        doc.save(`${currentDate}_estadisticas.pdf`);
      });
    }).catch((error) => {
      console.error('Error durante la generación del PDF:', error);
    });
  }
  
  
}