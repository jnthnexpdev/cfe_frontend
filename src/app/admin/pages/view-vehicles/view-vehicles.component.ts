import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehiclesService } from '../../services/vehicles/vehicles.service';
import { DataService } from '../../services/data/data.service';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { AuthService } from 'src/app/auth/services/auth/auth.service';

@Component({
  selector: 'app-view-vehicles',
  templateUrl: './view-vehicles.component.html',
  styleUrls: ['./view-vehicles.component.css', './view-extra.css']
})
export class ViewVehiclesComponent implements OnInit{

  currentMode : boolean = false;
  search_value = '';
  vehicles : any [] = [];
  currentPage = 1;
  totalPages : number = 0;
  userType: string = ''; 
  userDepartment : string = '';

  constructor(
    private router : Router,
    private vehicle : VehiclesService,
    private data : DataService,
    private alert : AlertService,
    private auth : AuthService
  ){}

  ngOnInit(): void {
    this.darkMode();

    this.auth.getUserType().subscribe((userType) => {
      this.userType = userType;
      const id = this.auth.getId();

      if(this.userType === 'Jefe'){

        this.auth.departmentUser(id).subscribe((data : any) => {
          this.userDepartment = data.manager?.Departamento;
          this.getVehiclesDepartment();
        });

      }else{
        this.getVehicles();
      }

    });
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  addVehicle(){
    setTimeout(() => {
      this.router.navigate(['/administrador/nuevo-vehiculo']).then(() => {});
    }, 1); 
  }

  getVehicles(){
    this.vehicle.getVehicles(this.currentPage).subscribe((data : any) => {
      this.vehicles = data.vehicles;
      this.currentPage = data.currentPage;
      this.totalPages = data.totalPages;
    });
  }

  getVehiclesDepartment(){
    this.vehicle.getVehiclesDeparment(this.userDepartment, this.currentPage).subscribe(
       (data: any) => {
          console.log(data);
          this.vehicles = data.vehicles;
       },
       (error: any) => {
          console.error('Error fetching vehicles:', error);
       }
    );
 }
 

  previousPage(){
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getVehicles();
    }
  }

  nextPage(){
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.getVehicles();
    }
  }

  isPreviousDisabled(): string | null {
    return this.currentPage === 1 ? 'disabled-button' : null;
  }
  
  isNextDisabled(): string | null {
    return this.currentPage === this.totalPages ? 'disabled-button' : null;
  }

  editVehicle(id : any){
    this.data.setIdVehicle(id);

    setTimeout(() => {
      this.router.navigate(['/administrador/editar-vehiculo', id]).then(() => {});
    }, 1);
  }

  deleteVehicle(id : any){
    this.data.setIdVehicle(id);
    this.alert.showConfirm('vehicle');
  }

  search(){
    if(this.search_value.trim() ===''){
      this.getVehicles();
    }else{
      this.vehicle.searchVehicle(this.search_value).subscribe((data : any) => {
        this.vehicles = data.vehicles;
      });
    }
  }
  
}