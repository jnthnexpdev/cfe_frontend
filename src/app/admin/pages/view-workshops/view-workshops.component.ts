import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { WorkshopService } from '../../services/workshop/workshop.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-view-workshops',
  templateUrl: './view-workshops.component.html',
  styleUrls: ['./view-workshops.component.css']
})
export class ViewWorkshopsComponent implements OnInit{

  currentMode: boolean = false;
  search_value = '';
  workshop : any [] = [];
  currentPage = 1;
  totalPages : number = 0;

  constructor(
    private router : Router,
    private mechanic : WorkshopService,
    private data : DataService,
    private alert : AlertService,
  )
  {}

  ngOnInit(): void {
    this.darkMode();
    this.getDataWorkshop();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  addWorkshop(){
    setTimeout(() => {
      this.router.navigate(['/administrador/nuevo-taller']).then(() => {});
    }, 1);
  }

  getDataWorkshop(){
    this.mechanic.getDataMechanicWorkshop(this.currentPage).subscribe((data : any) => {
      this.workshop = data.workshops;
      this.currentPage = data.currentPage;
      this.totalPages = data.totalPages;
    });
  }

  previousPage(){
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getDataWorkshop();
    }
  }

  nextPage(){
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.getDataWorkshop();
    }
  }

  isPreviousDisabled(): string | null {
    return this.currentPage === 1 ? 'disabled-button' : null;
  }
  
  isNextDisabled(): string | null {
    return this.currentPage === this.totalPages ? 'disabled-button' : null;
  }

  search(){
    if (this.search_value.trim() === '') {
      this.getDataWorkshop(); // Obtener todos los talleres si el campo de búsqueda está vacío
    } else {
      this.mechanic.searchWorkshop(this.search_value).subscribe((data: any) => {
        this.workshop = data.workshops;
      });
    }
  }

  editWorkshop(id : any){
    this.data.setIdWorkshop(id);
    
    setTimeout(() => {
      this.router.navigate(['/administrador/editar-taller', id]).then(() => {});
    }, 1);
  }

  deleteWorkshop(id : any){
    this.data.setIdWorkshop(id);
    this.alert.showConfirm('workshop');
  }

  detailsWorkshop(id : any){
    this.mechanic.detailsWorkshop(id);
  }

  removeSpaces(celular: string): string {
    return celular.replace(/\s/g, '');
  }

}