import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { WorkshopService } from '../../services/workshop/workshop.service';
import { VehiclesService } from '../../services/vehicles/vehicles.service';

@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.css', './add-extra.css']
})
export class AddVehicleComponent implements OnInit{
  
  vehicle : any = {};
  currentMode : Boolean = false;
  isLinear = false;
  days = ['L', 'M', 'M', 'J', 'V', 'S', 'D'];
  daysValue : string[] = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
  selectedDays: string[] = [];

  departmentForm = this.form.group({
    Telefono_Encargado: ['', [Validators.required, Validators.pattern(/^(\+\d{1,3})?[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})$/)]],
    Economico_Vehiculo : ['', Validators.required],
    RPE_Resguardante: ['', Validators.required],
    Nombre_Resguardante: ['', Validators.required],
    Numero_Activo: ['', Validators.required],
    Division: ['', Validators.required],
    Zona: ['', Validators.required],
    Departamento_Vehiculo: ['', Validators.required],
    Area: ['', Validators.required],
    Centro_Trabajo: ['', Validators.required],
  });

  vehicleForm = this.form.group({
    Dias_Descanso: [''],
    Financiamiento_Vehiculo: ['', Validators.required],
    Placas_Vehiculo: ['', Validators.required],
    Traccion_Vehiculo: ['', Validators.required],
    Combustible_Vehiculo: ['', Validators.required],
    Uso_Vehiculo : ['', Validators.required],
    Estatus_Vehiculo: ['', Validators.required],
    Estatus_MYSAP_Vehiculo: ['', Validators.required],
    Ubicacion_Fisica_Vehiculo: ['', Validators.required],
    Verificacion_Vehiculo: ['', Validators.required],
    Problema_Verificacion_Vehiculo : ['', Validators.required],
  });

  chasisForm = this.form.group({
    Marca_Chasis: ['', Validators.required],
    Submarca_Chasis: ['', Validators.required],
    Modelo_Chasis: ['', Validators.required],
    Numero_Serie_Chasis: ['', Validators.required],
    Tipo_Chasis: ['', Validators.required],
    
    Marca_Hidraulico: ['', Validators.required],
    Tipo_Hidraulico: ['', Validators.required],
    Submarca_Hidraulico: ['', Validators.required],
    Modelo_Hidraulico : ['', Validators.required],
  });

  constructor(
    private form : FormBuilder,
    private router : Router,
    private alert : AlertService,
    private _vehicle : VehiclesService
  ){}

  ngOnInit(): void {
    this.darkMode(); 
  }

  backTable(){
    setTimeout(() => {
      this.router.navigate(['/administrador/ver-vehiculos']).then(() => {});
    }, 1);
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  toggleDay(day: string) {
    const index = this.selectedDays.indexOf(day);

    if (index !== -1) {
      this.selectedDays.splice(index, 1);
    } else {
      this.selectedDays.push(day);
    }
    this.vehicleForm.get('Dias_Descanso')?.setValue(this.selectedDays.join(', '));
  }

  getForms(){
    this.vehicle = {
      ...this.departmentForm.value,
      ...this.vehicleForm.value,
      ...this.chasisForm.value
    };

    this.addVehicle();
  }

  addVehicle(){
    this._vehicle.addVehicle(this.vehicle).subscribe((data : any) => {
      if(data.ok === true){
        this.alert.showOk(data.message);

        setTimeout(() => {
          this.router.navigate(['/administrador/ver-vehiculos']).then(() => {
            window.location.reload();
          });
        }, 2100);
      }
    }, (err : any) => {
      this.alert.showError(err.error.message);
    });
  }

}
