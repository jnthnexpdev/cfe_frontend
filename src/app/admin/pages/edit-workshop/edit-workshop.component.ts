import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { DataService } from '../../services/data/data.service';
import { WorkshopService } from '../../services/workshop/workshop.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-workshop',
  templateUrl: './edit-workshop.component.html',
  styleUrls: ['./edit-workshop.component.css', './edit-extra.css']
})
export class EditWorkshopComponent implements OnInit{
  
  currentMode : Boolean = false;
  workshopData : any = {};
  workshopForm !: FormGroup;
  idWorkshop : string | null = '';

  constructor(
    private router : Router,
    private alert : AlertService,
    private data : DataService,
    private workshop : WorkshopService,
    private form : FormBuilder
  ){}

  ngOnInit(): void {
    this.darkMode();
    
    this.getId();
    this.getDataWorkshop();

    this.workshopForm = this.form.group({
      Nombre: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{3,50}$/)]],
      Nombre_Responsable: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ\s.\[\]_=+]{3,100}$/)]],
      Celular: ['', [Validators.required, Validators.pattern(/^(\+\d{1,3})?[-.\s]?(\d{1,})[-.\s]?(\d{1,})[-.\s]?(\d{1,})[-.\s]?(\d{1,})$/)]],
      Celular2: ['', [ Validators.pattern(/^(\+\d{1,3})?[-.\s]?(\d{1,})[-.\s]?(\d{1,})[-.\s]?(\d{1,})[-.\s]?(\d{1,})$/)]],
      Direccion: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{10,100}$/)]],
      Email: ['', [ Validators.pattern(/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9._%+-]+@[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9.-]+\.[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]{2,}$/)]],
      Zona: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{5,50}$/)]],
      Centro_Trabajo: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{5,50}$/)]],
    });
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  //get id workshop for send request and obtain data workshop
  getId(){
    this.idWorkshop = this.data.getIdWorkshop()
  }

  getDataWorkshop(){
    this.workshop.searchWorkshopID(this.idWorkshop).subscribe((data : any) => {
      this.workshopData = data.workshop;

      this.workshopForm.patchValue({
        Nombre: this.workshopData.Nombre,
        Nombre_Responsable: this.workshopData.Nombre_Responsable,
        Celular: this.workshopData.Celular,
        Celular2: this.workshopData.Celular2,
        Direccion: this.workshopData.Direccion,
        Email: this.workshopData.Email,
        Zona: this.workshopData.Zona,
        Centro_Trabajo: this.workshopData.Centro_Trabajo,
      });

    });
  }

  sendForm(){
    const formData = this.workshopForm.value;

    formData.Celular = formData.Celular.replace(/\s/g, '');
    formData.Celular2 = formData.Celular2.replace(/\s/g, '');

    if(this.workshopForm.valid){
      this.workshop.editWorkshop(this.idWorkshop, formData).subscribe((data : any) => {
        this.alert.showOk(data.message);
        setTimeout(() => {
          this.router.navigate(['/administrador/informacion-talleres']).then(() => {
            // window.location.reload();
          });
        }, 2050);
      }, (error : any) => {
        this.alert.showError(error.error.message);
      })
    }else{
      this.alert.showError('Revisa los campos');
    }
  }

  backTable(){
    setTimeout(() => {
      this.router.navigate(['/administrador/ver-talleres']).then(() => {});
    }, 1);
  }

}