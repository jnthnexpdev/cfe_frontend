import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRepairsComponent } from './view-repairs.component';

describe('ViewRepairsComponent', () => {
  let component: ViewRepairsComponent;
  let fixture: ComponentFixture<ViewRepairsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ViewRepairsComponent]
    });
    fixture = TestBed.createComponent(ViewRepairsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
