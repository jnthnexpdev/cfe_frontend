import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RepairService } from '../../services/repairs/repair.service';
import { PostService } from 'src/app/shared/services/posts/post.service';
import { switchMap } from 'rxjs';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { DataService } from '../../services/data/data.service';
import { RepairWorkshopService } from 'src/app/workshop/services/repair-workshop/repair-workshop.service';
import { ChartData, ChartOptions, ChartType } from 'chart.js';

@Component({
  selector: 'app-view-repairs',
  templateUrl: './view-repairs.component.html',
  styleUrls: ['./view-repairs.component.css']
})
export class ViewRepairsComponent implements OnInit{

  currentMode : Boolean = false;
  adminsData : any [] = [];
  vehiclesData : any [] = [];
  repairsData : any = [];
  workshopsData : any [] = [];
  search_value = '';
  currentPage = 1;
  totalPages : number = 0;

  valores = {
    var1 : 100,
    var2 : 90,
    var3 : 80,
    var4 : 70,
    var5 : 60,
    var6 : 50,
    var7 : 40,
    var8 : 30,
  }

  valoresArray: number[] = Object.values(this.valores);

  public barChartType: ChartType = 'pie'
  public barChartLabels: string[] = [`Departamento 1`, 'Label 2', 'Label 3', 'Label 4', 'Label 5', 'Label 6', 'Label 7', 'Label 8'];
  public barChartData: ChartData<'bar'> = {
    labels: this.barChartLabels,
    datasets: [
      {
        label: "Vehiculos",
        data: this.valoresArray,
        backgroundColor: ["#008e60", "#1446a0", "#9abe26", "#140152", "#f45866", "#f2e30f", "#bb0a21", "#3f8efc"],
        hoverBackgroundColor: ["#008e60", "#1446a0", "#9abe26", "#140152", "#f45866", "#f2e30f", "#bb0a21", "#3f8efc"],
      }
    ]
  };

  constructor(
    private router : Router,
    private repair : RepairService,
    private post : PostService,
    private alert : AlertService,
    private data : DataService,
    private repair_workshop : RepairWorkshopService
  ){}

  ngOnInit(): void {
    this.darkMode();
    this.getRepairs();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getRepairs(){
    this.repair.getRepairs(this.currentPage).subscribe((data : any) => {
      this.adminsData = data.admins;
      this.vehiclesData = data.vehicles;
      this.repairsData = data.repairs;
      this.workshopsData = data.workshops;
      this.totalPages = data.totalPages;
      this.currentPage = data.currentPage;
    });
  }

  previousPage(){
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getRepairs();
    }
  }

  nextPage(){
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.getRepairs();
    }
  }

  isPreviousDisabled(): string | null {
    return this.currentPage === 1 ? 'disabled-button' : null;
  }
  
  isNextDisabled(): string | null {
    return this.currentPage === this.totalPages ? 'disabled-button' : null;
  }
  
  addRepair(){
    setTimeout(() => {
      this.router.navigate(['/administrador/nueva-reparacion']).then(() => {});
    }, 1);
  }

  viewRepair(){
    setTimeout(() => {
      this.router.navigate(['/taller/reparacion/detalles-reparacion-vehiculo']).then(() => {});
    }, 1);
  }

  editRepair(id : any){
    this.data.setIdRepair(id);
    setTimeout(() => {
      this.router.navigate(['/administrador/editar-reparacion', id]).then(() => {})
    }, 1);
  }

  search(){
    if(this.search_value.trim() ===''){
      this.getRepairs();
    } else if(this.search_value.trim() === 'sin taller'){
      this.repair.repairsWithoutWorkshop(this.currentPage).subscribe((data : any) => {
        this.adminsData = data.admins;
        this.vehiclesData = data.vehicles;
        this.repairsData = data.repairs;
        this.workshopsData = data.workshops;
        this.totalPages = data.totalPages;
        this.currentPage = data.currentPage;
      });
    }else{
      this.repair.searchRepairs(this.currentPage, this.search_value).subscribe((data : any) => {
        this.adminsData = data.admins;
        this.vehiclesData = data.vehicles;
        this.repairsData = data.repairs;
        this.workshopsData = data.workshops;
        this.totalPages = data.totalPages;
        this.currentPage = data.currentPage;
      });
    }
  }

  deleteRepairAndPost(id : any, placas : any, workshop : any){
    this.data.setIdRepair(id);
    this.data.setPlates(placas);
    this.data.setIdWorkshop(workshop);
    this.alert.showConfirm('repair');
  }

  detailsRepair(id : any){
    this.repair_workshop.setIdRepair(id);
    
    setTimeout(() => {
      this.router.navigate(['/taller/reparacion/detalles-reparacion-vehiculo', id]).then(() => {});
    }, 5);
  }

}