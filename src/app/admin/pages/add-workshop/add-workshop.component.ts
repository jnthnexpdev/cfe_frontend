import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { WorkshopService } from '../../services/workshop/workshop.service';

@Component({
  selector: 'app-add-workshop',
  templateUrl: './add-workshop.component.html',
  styleUrls: ['./add-workshop.component.css', './add-extra.css']
})
export class AddWorkshopComponent implements OnInit{

  currentMode: boolean = false;
  workshopForm!: FormGroup;

  constructor(
    private form : FormBuilder,
    private router : Router,
    private alert : AlertService,
    private workshop : WorkshopService
  ){}

  ngOnInit(): void {
    this.darkMode();

    this.workshopForm = this.form.group({
      Nombre: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{3,50}$/)]],
      Nombre_Responsable: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ\s.\[\]_=+]{3,100}$/)]],
      Celular: ['', [Validators.required, Validators.pattern(/^(\+\d{1,3})?[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})$/)]],
      Celular2: ['', [Validators.pattern(/^(\+\d{1,3})?[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})$/)]],
      Direccion: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{10,100}$/)]],
      Email: ['', [ Validators.pattern(/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9._%+-]+@[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9.-]+\.[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]{2,}$/)]],
      Zona: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{5,50}$/)]],
      Centro_Trabajo: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{5,50}$/)]],
      Password: ['', [Validators.required, Validators.pattern(/^(?=.*[a-záéíóúüñ])(?=.*[A-ZÁÉÍÓÚÜÑ])(?=.*\d)[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9]{8,20}$/)]],
    });
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  backTable(){
    setTimeout(() => {
      this.router.navigate(['/administrador/ver-talleres']).then(() => {});
    }, 1);
  }

  addWorkshop() {
    const formData = this.workshopForm.value;

    formData.Celular = formData.Celular.replace(/\s/g, '');
    formData.Celular2 = formData.Celular2.replace(/\s/g, '');

    if (this.workshopForm.valid) {

      this.workshop.addMechanicWorkshop(formData).subscribe((data : any) => {
        this.alert.showOk(data.message);
        setTimeout(() => {
          this.router.navigate(['/administrador/ver-talleres']).then(() => {});
        }, 2100);
      }, (error: any) => {
        this.alert.showError(error.error.message);
      });

    } else {
      this.alert.showError('Revisa la información');
    }
    
  }

}