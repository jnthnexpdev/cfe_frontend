import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { MatStepperModule } from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatNativeDateModule} from '@angular/material/core';

import { AdminRoutingModule } from './admin-routing.module';
import { AddWorkshopComponent } from './pages/add-workshop/add-workshop.component';
import { EditWorkshopComponent } from './pages/edit-workshop/edit-workshop.component';
import { ViewWorkshopsComponent } from './pages/view-workshops/view-workshops.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from '../shared/interceptors/auth.interceptors';
import { DetailsWorkshopComponent } from './components/details-workshop/details-workshop.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewVehiclesComponent } from './pages/view-vehicles/view-vehicles.component';
import { DetailsVehiclesComponent } from './components/details-vehicles/details-vehicles.component';
import { AddVehicleComponent } from './pages/add-vehicle/add-vehicle.component';
import { ViewRepairsComponent } from './pages/view-repairs/view-repairs.component';
import { MatIconModule } from '@angular/material/icon';
import { EditVehicleComponent } from './pages/edit-vehicle/edit-vehicle.component';
import { AddRepairComponent } from './pages/add-repair/add-repair.component';
import { EditRepairComponent } from './pages/edit-repair/edit-repair.component';
import { NgChartsModule } from 'ng2-charts';
import { ChartsInfoComponent } from './pages/charts-info/charts-info.component';

@NgModule({
  declarations: [
    AddWorkshopComponent,
    EditWorkshopComponent,
    ViewWorkshopsComponent,
    DetailsWorkshopComponent,
    ViewVehiclesComponent,
    DetailsVehiclesComponent,
    AddVehicleComponent,
    ViewRepairsComponent,
    EditVehicleComponent,
    AddRepairComponent,
    EditRepairComponent,
    ChartsInfoComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    HttpClientModule,
    FormsModule,
    MatStepperModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    NgChartsModule
  ],
  providers : [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    DatePipe
  ]
})
export class AdminModule { }
