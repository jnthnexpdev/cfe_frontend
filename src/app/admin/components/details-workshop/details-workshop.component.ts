import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { WorkshopService } from '../../services/workshop/workshop.service';

@Component({
  selector: 'app-details-workshop',
  templateUrl: './details-workshop.component.html',
  styleUrls: ['./details-workshop.component.css']
})
export class DetailsWorkshopComponent implements OnInit{
  
  currentMode: boolean = false;
  workshop: any[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { id: string },
    private mechanic : WorkshopService,
    private dialog : MatDialog
  ){}

  ngOnInit(): void {
    this.getWorkshop();
    this.darkMode();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getWorkshop(){
    this.mechanic.searchWorkshopID(this.data.id).subscribe((data : any) => {
      this.workshop = [data.workshop];
    });
  }

  closeDetails(){
    this.dialog.closeAll();
  }

}