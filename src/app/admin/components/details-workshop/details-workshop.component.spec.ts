import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsWorkshopComponent } from './details-workshop.component';

describe('DetailsWorkshopComponent', () => {
  let component: DetailsWorkshopComponent;
  let fixture: ComponentFixture<DetailsWorkshopComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsWorkshopComponent]
    });
    fixture = TestBed.createComponent(DetailsWorkshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
