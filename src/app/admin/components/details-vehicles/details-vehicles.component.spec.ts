import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsVehiclesComponent } from './details-vehicles.component';

describe('DetailsVehiclesComponent', () => {
  let component: DetailsVehiclesComponent;
  let fixture: ComponentFixture<DetailsVehiclesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsVehiclesComponent]
    });
    fixture = TestBed.createComponent(DetailsVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
