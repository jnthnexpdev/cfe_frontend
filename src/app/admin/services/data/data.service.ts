import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public id_workshop = new BehaviorSubject<string | null>(null);
  public id_vehicle = new BehaviorSubject<string | null>(null);
  public id_repair = new BehaviorSubject<string | null>(null);
  public vehicle_plates = new BehaviorSubject<string | null>(null);
  
  constructor() { }

  setIdWorkshop(id : any){
    this.id_workshop.next(id);
  }

  setIdVehicle(id : any){
    this.id_vehicle.next(id);
  }

  setIdRepair(id : any){
    this.id_repair.next(id);
  }

  setPlates(plates : any){
    this.vehicle_plates.next(plates);
  }

  getIdVehicle(){
    return this.id_vehicle.getValue();
  }

  getIdWorkshop(){
    return this.id_workshop.getValue();
  }

  getIdRepair(){
    return this.id_repair.getValue();
  }

  getPlates(){
    return this.vehicle_plates.getValue();
  }
  
}
