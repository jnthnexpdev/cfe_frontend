import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DetailsWorkshopComponent } from '../../components/details-workshop/details-workshop.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkshopService {

  private url = 'http://localhost:3900/api/v1/cfe/workshop'

  constructor(
    private http: HttpClient,
    private dialog : MatDialog
  ){}

  getDataMechanicWorkshop(page : number) : Observable<any>{
    const pageSize = 2
    const url = `${this.url}/list`;
    const options = { 
      withCredentials: true,
      params: {
        page: page.toString(),
        pageSize: pageSize.toString()
      }
    };
    return this.http.get(url, options);
  }

  getNameWorkshops(){
    const url = `${this.url}/names`;
    const options = { withCredentials: true };
    return this.http.get(url, options);
  }

  addMechanicWorkshop(body : any){
    const url = `${this.url}/create-account`;
    const options = { withCredentials : true}
    return this.http.post(url, body, options);
  }

  searchWorkshop(value : any){
    const url = `${this.url}/search/${value}`;
    const options = { withCredentials : true}
    return this.http.get(url, options);
  }

  searchWorkshopID(id : any){
    const url = `${this.url}/search-id/${id}`;
    const options = { withCredentials : true}
    return this.http.get(url, options);
  }

  editWorkshop(id : any, body : any){
    const url = `${this.url}/edit/${id}`;
    const options = { withCredentials : true }
    return this.http.put(url, body, options);
  }

  deleteWorkshop(id : any){
    const url = `${this.url}/delete/${id}`;
    const options = { withCredentials : true }
    return this.http.delete(url, options);
  }

  detailsWorkshop(id : any){
    const dialogRef : MatDialogRef <DetailsWorkshopComponent> = this.dialog.open(DetailsWorkshopComponent, {
      data : {id : id}
    });
  }

}