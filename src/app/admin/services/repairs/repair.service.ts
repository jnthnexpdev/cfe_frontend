import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RepairService {

  private url : String = "http://localhost:3900/api/v1/cfe/repair";

  constructor(private http : HttpClient) { }

  addRepair(body: any, files: any) {
    const formData = new FormData();
    
    // Agregar campos de texto al FormData
    for (const key in body) {
      if (body.hasOwnProperty(key)) {
        formData.append(key, body[key]);
      }
    }
    
    // Agregar archivos al FormData
    for (let i = 0; i < files.length; i++) {
      formData.append('Imagenes_Vehiculo', files[i], files[i].name);
    }
  
    const options = { withCredentials: true };
    const url = `${this.url}/create-repair`;
  
    return this.http.post(url, formData, options);
  }

  getRepairs(page : number): Observable<any> {
    const pageSize = 2
    const url = `${this.url}/list`;
    const options = { 
      withCredentials: true,
      params: {
        page: page.toString(),
        pageSize: pageSize.toString()
      }
    };
    return this.http.get(url, options);
  }

  getStats(): Observable<any> {
    const url = `${this.url}/stats`;
    const options = { 
      withCredentials: true,
    };
    return this.http.get(url, options);
  }

  repairsWithoutWorkshop(page : number): Observable<any> {
    const pageSize = 2
    const url = `${this.url}/list-without-workshop`;
    const options = { 
      withCredentials: true,
      params: {
        page: page.toString(),
        pageSize: pageSize.toString()
      }
    };
    return this.http.get(url, options);
  }

  searchRepairs(page : number, value : any): Observable<any> {
    const pageSize = 2
    const url = `${this.url}/list-workshop/${value}`;
    const options = { 
      withCredentials: true,
      params: {
        page: page.toString(),
        pageSize: pageSize.toString()
      }
    };
    return this.http.get(url, options);
  }
  
  updateRepair(id : any, body : any){
    const url = `${this.url}/update/${id}`;
    const options = { withCredentials : true };
    return this.http.put(url, body, options);
  }

  confirmRepair(id_repair: any, id_vehicle: any) {
    const url = `${this.url}/confirm-repair/${id_repair}/${id_vehicle}`;
    const options = { withCredentials: true };
    return this.http.put(url, {}, options);
  }
  
  deleteRepairId(id_repair : any, placas : any, workshop : any){
    const url = `${this.url}/delete/${id_repair}/${placas}/${workshop}`;
    const options = { withCredentials : true };
    return this.http.delete(url, options);
  }
  
}