import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DetailsVehiclesComponent } from '../../components/details-vehicles/details-vehicles.component';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  constructor(
    private dialog : MatDialog,
    private http : HttpClient
  ){}

  private url = 'http://localhost:3900/api/v1/cfe/vehicle'

  addVehicle(body : any){
    const url = `${this.url}/new-vehicle`;
    const options = { withCredentials : true };
    return this.http.post(url, body, options);
  }

  getVehicles(page : number) : Observable<any>{
    const pageSize = 2
    const url = `${this.url}/list`;
    const options = { 
      withCredentials: true,
      params: {
        page: page.toString(),
        pageSize: pageSize.toString()
      }
    };
    return this.http.get(url, options);
  }

  getVehiclesDeparment(value : any, page : number) : Observable<any>{
    console.log('value:', value)
    const pageSize = 2
    const url = `${this.url}/search/department/${value}`;
    const options = { 
      withCredentials: true,
      params: {
        page: page.toString(),
        pageSize: pageSize.toString()
      }
    };
    return this.http.get(url, options);
  }

  editVehicle(id : any, body : any){
    const url = `${this.url}/edit/${id}`;
    const options = { withCredentials : true };
    return this.http.put(url, body, options);
  }

  searchVehicle(value : any){
    const url = `${this.url}/search/${value}`;
    const options = {withCredentials : true};
    return this.http.get(url, options);
  }


  searchVehicleID(id : any){
    const url = `${this.url}/search-id/${id}`;
    const options = {withCredentials : true};
    return this.http.get(url, options);
  }

  detailsVehicles(id : any){
    const dialogRef : MatDialogRef <DetailsVehiclesComponent> = this.dialog.open(DetailsVehiclesComponent, {
      data : {id : id}
    });
  }

  deleteVehicle(id : any){
    const url = `${this.url}/delete/${id}`;
    const options = {withCredentials : true};
    return this.http.delete(url, options);
  }
  
}