import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  currentMode: boolean = false;
  title = 'taller_frontend';

  ngOnInit(): void {
    this.darkMode();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  colorMode() {
    const currentMode = localStorage.getItem('darkMode') == 'true';
    localStorage.setItem('darkMode', currentMode ? 'false' : 'true');
    
    setTimeout(() => {
      window.location.reload();
    }, 100);
  }

  scrollTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' }); // Desplazamiento suave hacia arriba
  }

  scrollBottom() {
    const scrollHeight = document.documentElement.scrollHeight;
    window.scrollTo({ top: scrollHeight, behavior: 'smooth' }); // Desplazamiento suave hacia abajo
  }

}