import { DialogRef } from '@angular/cdk/dialog';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { RepairWorkshopService } from '../../services/repair-workshop/repair-workshop.service';
import { DataService } from 'src/app/admin/services/data/data.service';


@Component({
  selector: 'app-add-step',
  templateUrl: './add-step.component.html',
  styleUrls: ['./add-step.component.css']
})
export class AddStepComponent implements OnInit{

  currentMode : Boolean | null = false;
  files: File[] = [];
  formStep !: FormGroup; 
  id : String | null = '';

  constructor(
    private dialogRef : DialogRef <AddStepComponent>,
    private form : FormBuilder,
    private alert : AlertService,
    private repair : RepairWorkshopService,
  ){}

  ngOnInit(): void {
    this.darkMode();

    this.formStep = this.form.group({
      Titulo_Etapa : ['', Validators.required],
      Descripcion_Etapa : ['', Validators.required],
      Imagenes_Etapa : ['', Validators.required],
    });
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  onFileChange(event: any): void {
    const selectedFiles = event.target.files;
  
    for (let i = 0; i < selectedFiles.length; i++) {
      this.files.push(selectedFiles[i]);
    }
  }
  
  removeFile(index: number): void {
    this.files.splice(index, 1);
  }

  closeForm(){
    this.dialogRef.close();
  }

  getId(){
    this.id = this.repair.getIdRepair();
  }

  getForm(){
    this.getId();
    const stepData = this.formStep.value;

    if(this.formStep.valid){

      this.repair.addStep(this.id, stepData, this.files).subscribe((data : any) => {
        if(data.ok === true){
          this.alert.showOk(data.message);
        }

        setTimeout(() => {
          window.location.reload();
        }, 2100);

      }, (err : any) => {
        this.alert.showError(err.error.message);
      });

    }else{
      this.alert.showError('Revisa y/o completa la informacion');
    }
  }

}