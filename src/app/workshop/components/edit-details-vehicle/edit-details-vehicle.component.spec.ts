import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDetailsVehicleComponent } from './edit-details-vehicle.component';

describe('EditDetailsVehicleComponent', () => {
  let component: EditDetailsVehicleComponent;
  let fixture: ComponentFixture<EditDetailsVehicleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditDetailsVehicleComponent]
    });
    fixture = TestBed.createComponent(EditDetailsVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
