import { DialogRef } from '@angular/cdk/dialog';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/admin/services/data/data.service';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { RepairWorkshopService } from '../../services/repair-workshop/repair-workshop.service';

@Component({
  selector: 'app-edit-details-vehicle',
  templateUrl: './edit-details-vehicle.component.html',
  styleUrls: ['./edit-details-vehicle.component.css']
})
export class EditDetailsVehicleComponent implements OnInit{

  currentMode : Boolean | null = false;
  vehicleForm !: FormGroup;
  id_repair : string | null = '';
  picker2 : string | null = null;
  
  constructor(
    private form : FormBuilder,
    private alert : AlertService,
    private repair : RepairWorkshopService,
    private dialogRef : DialogRef <EditDetailsVehicleComponent>,
    private datePipe: DatePipe 
  ){}

  ngOnInit(): void {
    this.darkMode();

    this.vehicleForm = this.form.group({
      Presupuesto_Final : ['', Validators.required],
      Fecha_Entrega: [this.datePipe.transform(new Date(), 'dd/MM/yyyy'), Validators.required],
    });

    this.getId();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getId(){
    this.id_repair = this.repair.getIdRepair();
  }

  closeForm(){
    this.dialogRef.close();
  }

  updateRepair() {
    const fechaSeleccionada = this.vehicleForm.value.Fecha_Entrega;
  
    if (fechaSeleccionada instanceof Date && !isNaN(fechaSeleccionada.getTime())) {
      // Formatear la fecha al formato 'dd/MM/yyyy'
      const fechaFormateada = this.datePipe.transform(
        fechaSeleccionada,
        'dd/MM/yyyy'
      );
  
      const dataRepair = {
        Presupuesto_Final: this.vehicleForm.value.Presupuesto_Final,
        Fecha_Entrega: fechaFormateada,
      };
  
      if (this.vehicleForm.valid) {
        this.repair.updateDetails(this.id_repair, dataRepair).subscribe((data : any) => {
          if(data.ok === true){
            this.alert.showOk(data.message);
            setTimeout(() => {
              window.location.reload();
            },2100);
          }
        }, (err : any) => {
          this.alert.showError(err.error.message);
        })
      } else {
        this.alert.showError('Ingresa la información correspondiente');
      }
    } else {
      this.alert.showError('Fecha no válida');
    }
  }
  
}