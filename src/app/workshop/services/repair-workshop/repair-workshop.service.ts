import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RepairWorkshopService {
  private url : string = 'http://localhost:3900/api/v1/cfe/repair';

  constructor(
    private http : HttpClient
  ){}

  setIdRepair(id : any){
    localStorage.setItem('reparacion', id);
  }

  getIdRepair(){
    return localStorage.getItem('reparacion');
  }

  setIdVehicle(id : any){
    localStorage.setItem('vehiculo', id);
  }

  getIdVehicle(){
    return localStorage.getItem('vehiculo');
  }

  confirmArrival(body : any){
    const url = `${this.url}/confirm-arrival`;
    const options = { withCredentials : true };
    return this.http.put(url, body, options);
  }

  updateDetails(id : any, body : any){
    const url = `${this.url}/submission/${id}`;
    const options = { withCredentials : true };
    return this.http.put(url, body, options);
  }

  addStep(id : any, body : any, files: any){
    const formData = new FormData();
    
    // Agregar campos de texto al FormData
    for (const key in body) {
      if (body.hasOwnProperty(key)) {
        formData.append(key, body[key]);
      }
    }

    // Agregar archivos al FormData
    for (let i = 0; i < files.length; i++) {
      formData.append('Imagenes_Etapa', files[i], files[i].name);
    }


    const url = `${this.url}/add-step/${id}`;
    const options = { withCredentials : true };
    return this.http.put(url, formData, options);
  }
}
