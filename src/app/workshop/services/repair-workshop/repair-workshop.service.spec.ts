import { TestBed } from '@angular/core/testing';

import { RepairWorkshopService } from './repair-workshop.service';

describe('RepairWorkshopService', () => {
  let service: RepairWorkshopService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RepairWorkshopService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
