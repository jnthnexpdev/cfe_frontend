import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WorkshopService {

  private url : string = 'http://localhost:3900/api/v1/cfe/workshop';

  constructor(
    private http : HttpClient
  ){}

  getRepairs(id : any){
    const url = `${this.url}/repairs/${id}`;
    const options = { withCredentials : true };
    return this.http.get(url, options);
  }

  searchRepair(id : any, value : any){
    const url = `${this.url}/search-vehicle-repair/${id}/${value}`;
    const options = { withCredentials : true };
    return this.http.get(url, options);
  }

  getRepair(id : any){
    const url = `${this.url}/repair/${id}`;
    const options = { withCredentials : true };
    return this.http.get(url, options);
  }

}