import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTooltipModule } from '@angular/material/tooltip';

import { WorkshopRoutingModule } from './workshop-routing.module';
import { RepairVehiclesComponent } from './pages/repair-vehicles/repair-vehicles.component';
import { DetailsRepairVehicleComponent } from './pages/details-repair-vehicle/details-repair-vehicle.component';
import { EditDetailsVehicleComponent } from './components/edit-details-vehicle/edit-details-vehicle.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { AddStepComponent } from './components/add-step/add-step.component';

@NgModule({
  declarations: [
    RepairVehiclesComponent,
    DetailsRepairVehicleComponent,
    EditDetailsVehicleComponent,
    AddStepComponent
  ],
  imports: [
    CommonModule,
    WorkshopRoutingModule,
    FormsModule,
    MatStepperModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    ReactiveFormsModule
  ]
})

export class WorkshopModule { }