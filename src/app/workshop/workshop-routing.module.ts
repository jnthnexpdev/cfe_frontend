import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepairVehiclesComponent } from './pages/repair-vehicles/repair-vehicles.component';
import { WorkshopGuard } from '../auth/guards/workshop.guard';
import { AuthGuard } from '../auth/guards/auth.guard';
import { DetailsRepairVehicleComponent } from './pages/details-repair-vehicle/details-repair-vehicle.component';

const routes: Routes = [
  {
    path : 'vehiculos',
    children : [
      {path : 'vehiculos-en-reparacion', component : RepairVehiclesComponent},
      {path : '**', redirectTo : 'vehiculos-en-reparacion', pathMatch : 'full'}
    ],
    canActivate : [WorkshopGuard]
  },
  {
    path : 'reparacion',
    children : [
      {path : 'detalles-reparacion-vehiculo/:id', component : DetailsRepairVehicleComponent},
      {path : '**', redirectTo : 'detalles-reparacion-vehiculo', pathMatch : 'full'}
    ],
    canActivate : [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkshopRoutingModule { }