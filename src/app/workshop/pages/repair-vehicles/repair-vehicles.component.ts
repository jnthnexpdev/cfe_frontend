import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth/auth.service';
import { WorkshopService } from '../../services/workshop/workshop.service';
import { RepairWorkshopService } from '../../services/repair-workshop/repair-workshop.service';


@Component({
  selector: 'app-repair-vehicles',
  templateUrl: './repair-vehicles.component.html',
  styleUrls: ['./repair-vehicles.component.css']
})
export class RepairVehiclesComponent implements OnInit{

  currentMode : Boolean = false;
  id : string | null = '';
  workshopData : any = {}
  repairsData : any [] = [];
  vehiclesData : any [] = [];
  search_value = '';

  constructor(
    private auth : AuthService,
    private router : Router,
    private workshop : WorkshopService,
    private repair : RepairWorkshopService
  ){}

  ngOnInit(): void {
    this.getId();
    this.getData();
    this.darkMode();
    this.getRepairs();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getId(){
    this.id = this.auth.getId();
  }

  getData(){
    this.auth.infoUser(this.id).subscribe((data : any) => {
      this.workshopData = [data.workshop];
    });
  }

  search(){
    if (this.search_value.trim() === '') {
      this.getRepairs(); // Obtener todos los talleres si el campo de búsqueda está vacío
    } else {
      this.workshop.searchRepair(this.id, this.search_value).subscribe((data: any) => {
        this.vehiclesData = data.vehicles;
        this.repairsData = data.repairs;
      });
    }
  }

  detailsRepair(id : any){
    this.repair.setIdRepair(id);
    
    setTimeout(() => {
      this.router.navigate(['/taller/reparacion/detalles-reparacion-vehiculo', id]).then(() => {});
    }, 5);
  }

  getRepairs(){
    this.workshop.getRepairs(this.id).subscribe((data : any) => {
      this.repairsData = data.repairs;
      this.vehiclesData = data.vehicles;
    });
  }

}