import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairVehiclesComponent } from './repair-vehicles.component';

describe('RepairVehiclesComponent', () => {
  let component: RepairVehiclesComponent;
  let fixture: ComponentFixture<RepairVehiclesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RepairVehiclesComponent]
    });
    fixture = TestBed.createComponent(RepairVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
