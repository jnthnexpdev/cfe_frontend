import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsRepairVehicleComponent } from './details-repair-vehicle.component';

describe('DetailsRepairVehicleComponent', () => {
  let component: DetailsRepairVehicleComponent;
  let fixture: ComponentFixture<DetailsRepairVehicleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsRepairVehicleComponent]
    });
    fixture = TestBed.createComponent(DetailsRepairVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
