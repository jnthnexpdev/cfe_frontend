import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { WorkshopService } from '../../services/workshop/workshop.service';
import { MatDialog } from '@angular/material/dialog';
import { EditDetailsVehicleComponent } from '../../components/edit-details-vehicle/edit-details-vehicle.component';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { DataService } from 'src/app/admin/services/data/data.service';
import { RepairWorkshopService } from '../../services/repair-workshop/repair-workshop.service';
import { AddStepComponent } from '../../components/add-step/add-step.component';
import { AuthService } from 'src/app/auth/services/auth/auth.service';

@Component({
  selector: 'app-details-repair-vehicle',
  templateUrl: './details-repair-vehicle.component.html',
  styleUrls: ['./details-repair-vehicle.component.css']
})
export class DetailsRepairVehicleComponent implements OnInit{

  isLinear = false;

  currentMode : Boolean = false;
  id_repair : string | null = '';
  repairData : any = {};
  vehicleData : any [] = [];
  filesData : any = {};
  userType: string = ''; 

  constructor(
    private form : FormBuilder,
    private repair : RepairWorkshopService,
    private workshop : WorkshopService,
    private dialog : MatDialog,
    private alert : AlertService,
    private data : DataService,
    private auth : AuthService
  ){}

  ngOnInit(): void {
    this.darkMode();
    this.getIdRepair();
    this.getData();
    this.getUserType();
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  getUserType(){
    this.auth.getUserType().subscribe((userType) => {
      this.userType = userType;
    });
  }

  getIdRepair(){
    this.id_repair = this.repair.getIdRepair();
    console.log(this.id_repair);
  }

  getData(){
    this.workshop.getRepair(this.id_repair).subscribe((data : any) => {
      this.repairData = data.repair;
      this.vehicleData = data.vehicle;
      this.filesData = data.files;
      console.log('Reparacion: ', this.repairData);
      console.log('Vehiculo: ', this.vehicleData);
      console.log('Archivos: ', this.filesData);
    });
  }

  openForm(){
    this.dialog.open(EditDetailsVehicleComponent);
  }

  confirmArrival(id_repair : any){
    this.data.setIdRepair(id_repair);
    this.alert.showConfirm('vehicle-arrive')
  }

  addStep(){
    this.dialog.open(AddStepComponent);
  }

  confirmRepair(id_repair : any, id_vehicle : any){
    this.repair.setIdRepair(id_repair);
    this.repair.setIdVehicle(id_vehicle);
    this.alert.showConfirm('confirm-repair');
  }

}