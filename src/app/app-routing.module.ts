import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './auth/guards/admin.guard';
import { AuthGuard } from './auth/guards/auth.guard';
import { WorkshopGuard } from './auth/guards/workshop.guard';

const routes: Routes = [
  {
    path : 'acceso',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path : 'administrador',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
    canActivate : [AdminGuard]
  },
  {
    path : 'seccion',
    loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule),
    canActivate : [AuthGuard]
  },
  {
    path : 'taller',
    loadChildren: () => import('./workshop/workshop.module').then(m => m.WorkshopModule),
    canActivate : [AuthGuard]
  },
  {
    path : '**',
    redirectTo : 'acceso',
    pathMatch : 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }