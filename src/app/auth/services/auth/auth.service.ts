// authService.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie';
import { Router } from '@angular/router';

export enum UserType {
  Administrador = 'Administrador',
  Mecanico = 'Mecanico',
  Jefe = "Jefe"
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private api = 'http://localhost:3900/api/v1/cfe/auth';

  constructor(
    private http: HttpClient, 
    private cookie: CookieService,
    private router: Router
  ){}

  createAccountAdmin(body : any){
    const url = `http://localhost:3900/api/v1/cfe/admin/create-account`;
    const options = { withCredentials: true };
    return this.http.post(url, body, options);
  }

  createAccountManager(body : any){
    const url = `http://localhost:3900/api/v1/cfe/manager/create-account`;
    const options = { withCredentials: true };
    return this.http.post(url, body, options);
  }

  loginAuth(body: any): Observable<any> {
    const url = `${this.api}/login`;
    const options = { withCredentials: true };
    return this.http.post(url, body, options);
  }

  infoUser(id : any){
    const url = `${this.api}/user/${id}`;
    const options = { withCredentials: true };
    return this.http.get(url, options);
  }

  departmentUser(id : any){
    const url = `${this.api}/user/department/${id}`;
    const options = { withCredentials: true };
    return this.http.get(url, options);
  }

  isAuthenticated(): Observable<boolean> {
    const token = this.cookie.get('token');
    return of(!!token);
  }

  getUserType(): Observable<string> {
    const url = `${this.api}/user/type`;
    const token = this.cookie.get('token');
    
    if (!token) {
      return of('');
    }
  
    const headers = { Authorization: `Bearer ${token}` };
  
    return this.http.get<{ user: string }>(url, { headers, withCredentials: true })
      .pipe(
        map(response => response.user),
        catchError(error => {
          this.logOut();
          console.error('Error al obtener el tipo de usuario:', error);
          return of('');
        })
      );
  }

  getId(): string  | null{
    const token = localStorage.getItem('token');

    if(token){
      const decoded = JSON.parse(atob(token.split('.')[1]));
      const id = decoded.userId;

      return id;
    }else{
      return null;
    }
  }

  private mapUserType(userTypeString: string): UserType {
    switch (userTypeString) {
      case 'Administrador':
        return UserType.Administrador;
      case 'Taller':
        return UserType.Mecanico;
      case 'Jefe':
        return UserType.Jefe;
      default:
        return UserType.Administrador; // O cualquier valor predeterminado que desees
    }
  }

  logOut(){
    this.cookie.removeAll();
    localStorage.removeItem('token');
    setTimeout(() => {
      this.router.navigate(['/acceso/inicio/iniciar-sesion']).then(() => {
        window.location.reload();
      });
    }, 1);  
  }

}