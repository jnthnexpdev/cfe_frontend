import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AdminGuard } from './guards/admin.guard';
import { RegisterComponent } from './pages/register/register.component';

const routes: Routes = [
  {
    path : 'inicio',
    children : [
      {path : 'iniciar-sesion', component : LoginComponent},
      {path : '**', redirectTo : 'iniciar-sesion', pathMatch : 'full'}
    ],
  },
  {
    path : 'registro',
    children : [
      {path : 'crear-cuenta', component : RegisterComponent},
      {path : '**', redirectTo : 'crear-cuenta', pathMatch : 'full'}
    ],
    canActivate : [AdminGuard]
  },
  {
    path : '**',
    redirectTo : 'inicio'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
