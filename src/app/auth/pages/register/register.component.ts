import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WorkshopService } from 'src/app/admin/services/workshop/workshop.service';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css', './register-extra.css']
})
export class RegisterComponent  implements OnInit{

  currentMode: boolean = false;
  userForm!: FormGroup;

  constructor(
    private form : FormBuilder,
    private router : Router,
    private alert : AlertService,
    private auth : AuthService
  ){}

  ngOnInit(): void {
    this.darkMode();

    this.userForm = this.form.group({
      Nombre: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{3,50}$/)]],
      RPE: ['', [Validators.required, Validators.pattern(/^[A-Za-záéíóúüñÁÉÍÓÚÜÑ0-9\s\-.,:;\/"#$()[\]_=+]{3,50}$/)]],
      Cuenta : [Validators.required],
      Departamento : [Validators.required],
      Celular: ['', [Validators.required, Validators.pattern(/^(\+\d{1,3})?[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})[\s]?(\d{1,})$/)]],
      Email: ['', [ Validators.pattern(/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9._%+-]+@[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9.-]+\.[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]{2,}$/)]],
      Password: ['', [Validators.required, Validators.pattern(/^(?=.*[a-záéíóúüñ])(?=.*[A-ZÁÉÍÓÚÜÑ])(?=.*\d)[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9]{8,20}$/)]],
    });
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  backTable(){
    setTimeout(() => {
      this.router.navigate(['/seccion/feed/publicaciones']).then(() => {});
    }, 1);
  }

  addUser() {
    const formData = this.userForm.value;

    formData.Celular = formData.Celular.replace(/\s/g, '');

    if (this.userForm.valid) {

      if(formData.Cuenta === 'Administrador'){
        this.auth.createAccountAdmin(formData).subscribe((data : any) => {
          this.alert.showOk(data.message);
          setTimeout(() => {
            this.router.navigate(['/acceso/registro/crear-cuenta']).then(() => {});
          }, 2100);
        }, (error: any) => {
          this.alert.showError(error.error.message);
        });
      }else if(formData.Cuenta == 'Jefe'){
        this.auth.createAccountManager(formData).subscribe((data : any) => {
          this.alert.showOk(data.message);
          setTimeout(() => {
            this.router.navigate(['/acceso/registro/crear-cuenta']).then(() => {});
          }, 2100);
        }, (error: any) => {
          this.alert.showError(error.error.message);
        });
      }


    } else {
      this.alert.showError('Revisa la información');
    }
    
  }

}