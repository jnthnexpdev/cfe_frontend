import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { AlertService } from 'src/app/shared/services/alerts/alert.service';
import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', './login-extra.css']
})
export class LoginComponent implements OnInit{
  currentMode: boolean = false;
  userType!: string;
  loginForm!: FormGroup;

  constructor(
    private formBuilder : FormBuilder,
    private router : Router,
    private login : AuthService,
    private alert : AlertService,
    private cookie : CookieService
  ){}

  ngOnInit(): void {
    this.darkMode();
    

    this.loginForm = this.formBuilder.group({
      Username: ['', [Validators.required, Validators.pattern(/^(?!.*\.{2,})(?!.*[@.]$)[a-zA-ZáéíóúÁÉÍÓÚüÜ0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$|^(?!.*[.:@])[0-9]{10}$|^[a-zA-ZáéíóúÁÉÍÓÚüÜ0-9]{3,10}$/)]],
      Password: ['', [Validators.required]],
    });
  }

  darkMode(){
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  verifyForm() {
    //Si el formulario es valido
    if (this.loginForm.valid) {
      this.sendForm();
    } else {
      this.alert.showError('Revisa tus datos');
    }
  }

  sendForm(){
    const formData = this.loginForm.value;

    this.login.loginAuth(formData).subscribe((data : any) => {
      //Si la respuesta del servidor es ok
      if(data.ok === true){
        this.alert.showOk('Sesión iniciada');
        localStorage.setItem('token', data.token);
      
        setTimeout(() => {
          this.router.navigate(['/seccion/feed/publicaciones']).then(() => {
            window.location.reload();
          });
        }, 2100);  
      }
    }, (err : any) => {
      this.alert.showError(err.error.message);
    });
  }

  logOut(){
    this.cookie.removeAll();
    localStorage.removeItem('token');
  }
  
}