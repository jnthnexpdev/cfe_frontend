// admin-guard.service.ts
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(
    private authService: AuthService, 
    private router: Router,
    private cookie: CookieService
) {}

  canActivate(): Observable<boolean> {
    const token = this.cookie.get('token');

    if (token) {
        return this.authService.getUserType().pipe(
            map(userType => {
                // Lógica para permitir o denegar el acceso según el tipo de usuario
                if (userType === 'Administrador') {
                    return true;
                } else if (userType === 'Jefe') {
                  return true;
                }
                else {
                    // Redirigir o denegar acceso
                    this.router.navigate(['/inicio/feed']);
                    return false;
                }
            })
        );
    }

    // Redirigir o denegar acceso si no hay token
    this.router.navigate(['/inicio/feed']);
    return of(false);
  }

}
